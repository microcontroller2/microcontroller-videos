#include <Arduino.h>

uint8_t pb1 = 13;
uint8_t pb2 = 12;
uint8_t pb3 = 11;

uint8_t green_led = 7;
uint8_t yellow_led = 6;
uint8_t red_led = 5;

void setup() {
  pinMode(pb1, INPUT);
  pinMode(pb2, INPUT);
  pinMode(pb3, INPUT);

  pinMode(green_led, OUTPUT);
  pinMode(yellow_led, OUTPUT);
  pinMode(red_led, OUTPUT);
}

void loop() {

//  digitalWrite(green_led, HIGH);
//  delay(200);
//  digitalWrite(green_led, LOW);
//  delay(200);
//
//  if(!digitalRead(pb1) || digitalRead(pb2)){
//    digitalWrite(green_led, HIGH);
//  }
//  else {
//    digitalWrite(green_led, LOW);
//  }
//
//  static uint8_t pb1_old_state = 0;
//  uint8_t pb1_state = digitalRead(pb1);
//
//  if(pb1_old_state != pb1_state){
//    pb1_old_state = pb1_state;
//
//    if(pb1_state == HIGH){ // Stigande flanke
//      digitalWrite(yellow_led, !digitalRead(yellow_led));
//    }
//    else { // Fallande flanke
//      digitalWrite(green_led, !digitalRead(green_led));
//    }
//    delay(50);
//  }


  if(digitalRead(pb3)){
    digitalWrite(red_led, HIGH);
  }
  else{
    digitalWrite(red_led, LOW);
  }

}