#include <Arduino.h>

const char* hest = "                    "
   "             |\\    "
   "/|\r\n"
   "                    "
   "          ___| \\,,/"
   "_/\r\n"
   "                    "
   "       ---__/ \\/   "
   " \\\r\n"
   "                    "
   "      __--/     (D) "
   " \\\r\n"
   "                    "
   "      _ -/    (_    "
   "  \\\r\n"
   "                    "
   "     // /       \\_ "
   "/  -\\\r\n"
   "   __-------_____--_"
   "__--/           / \\"
   "_ O o)\r\n"
   "  /                 "
   "                /   "
   "\\__/\r\n"
   " /                  "
   "               /\r\n"
   "||          )       "
   "            \\_/\\\r"
   "\n"
   "||         /        "
   "      _      /  |\r"
   "\n"
   "| |      /--______  "
   "    ___\\    /\\  :"
   "\r\n"
   "| /   __-  - _/   --"
   "----    |  |   \\ \\"
   "\r\n"
   " |   -  -   /       "
   "         | |     \\ "
   ")\r\n"
   " |  |   -  |        "
   "         | )     | |"
   "\r\n"
   "  | |    | |        "
   "         | |    | |"
   "\r\n"
   "  | |    < |        "
   "         | |   |_/\r"
   "\n"
   "  < |    /__\\      "
   "          <  \\\r\n"
   "  /__\\             "
   "          /___\\\r\n"
   "\r\n";

void setup() {
  Serial.begin(9600,SERIAL_8N1);
}

void loop() {
 // Serial.print("Dette er ein test.");
//
//  Serial.print("Dette er eit tal:");
//  Serial.println(65);
//  Serial.write(65);
////
//  Serial.println(48, HEX);
//  Serial.println(48, OCT);
//  Serial.println(48, BIN);
//  Serial.println(48, DEC);
//
  Serial.println(45.1234,3);

  for(uint8_t i = 0; i < 26; i++){
    Serial.write(i + 65);
    Serial.print(" => ");
    Serial.println(i + 65);
  }

  Serial.println("Dette er ein hest:");
  Serial.print(hest);

  delay(6000);
}